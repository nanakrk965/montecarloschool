﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace monteCarlo
{
    class monteCarlo
    {
        public static bool IsHit(int percent, Random random)
        {
            return !(random.Next(100) >= percent);
        }

        public static bool IsAllHit(int percent, int S, Random random)
        {
            for (int i = 0; i < S; i++)
            {
                if (!IsHit(percent, random))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsAnyHit(int percent, int S, Random random)
        {
            for (int i = 0; i < S; i++)
            {
                if (IsHit(percent, random))
                {
                    return true;
                }
            }
            return false;
        }

        public static int HitCount(int percent, int S, Random random)
        {
            var result = 0;
            for (int i = 0; i < S; i++)
            {
                if (IsHit(percent, random))
                {
                    result += 1; 
                }
            }
            return result;
        }

        //PパーセントがN回連続で当たる確率
        public static float Pattern1(int percent, int N, int simuNum, Random random)
        {
            int count = 0;

            for (int i = 0; i < simuNum; i++)
            {
                if (IsAllHit(percent, N, random))
                {
                    count += 1;
                       
                }
            }
            return ((float)count / (float)simuNum) * 100;
        }

        public static int FirstNotHitNum(int percent, Random random)
        {
            if (percent == 100.0) {
                Environment.Exit(-1);
            }
 
            int count = 1;
            while (true)
            {
                if (!IsHit(percent, random))
                {
                    break;
                }
                count += 1;
            }
            return count;
        }

        //Pパーセントが初めて外れる平均回数
        public static float Pattern2(int percent, int simuNum, Random random)
        {
            int result = 0;
            for (int i = 0; i < simuNum; i++)
            {
                result += FirstNotHitNum(percent, random);
            }
            return (float)result / (float)simuNum;
        }

        //PパーセントをS回試行してH回以上当たる確率
        public static float Pattern3(int percent, int S, int H, int simuNum, Random random)
        {
            int count = 0;
            for (int i = 0; i < simuNum; i++)
            {
                if (HitCount(percent, S, random) >= H)
                {
                    count += 1;
                }
            }
            return ((float)count / (float)simuNum) * 100;
        }

        //PパーセントをS回試行してH回丁度当たる確率
        public static float Pattern4(int percent, int S, int H, int simuNum, Random random)
        {
            int count = 0;
            for (int i = 0; i < simuNum; i++)
            {
                if (HitCount(percent, S, random) == H)
                {
                    count += 1;
                }
            }
            return ((float)count / (float)simuNum) * 100;
        }

        public static List<bool> CreateDeck(int deckNum, int hitNum)
        {
            var result = new List<bool>();
            for (int i = 0; i < hitNum; i++)
            {
                result.Add(true);
            }

            for (int i = 0; i < (deckNum - hitNum); i++)
            {
                result.Add(false);
            }
            return result;
        }

        public static List<bool> DeckShuffle(List<bool> deck )
        {
            return deck.OrderBy(ele => Guid.NewGuid()).ToList();
        }

        public static int DrawHitNum(List<bool> deck, int drawNum)
        {
            var result = 0;
            for (int i = 0; i < drawNum; i++)
            {
                if (deck[i])
                {
                    result += 1;
                }
            }
            return result;
        }

        //deckNum枚の中からhitNum枚当たりがある そこからdrawNum枚引いて当たりがH枚以上ある確率
        public static float Pattern5(int deckNum, int hitNum, int drawNum, int H, int simuNum, Random random)
        {
            int count = 0;
            List<bool> deck = CreateDeck(deckNum, hitNum);
            for (int i = 0; i < simuNum; i++)
            {
                deck = DeckShuffle(deck);
                if (DrawHitNum(deck, drawNum) >= H)
                {
                    count += 1;
                } 
            }
            return ((float)count / (float)simuNum) * 100;
        }

        //deckNum枚の中からhitNum枚当たりがある そこからdrawNum枚引いて当たりがH枚丁度ある確率
        public static float Pattern6(int deckNum, int hitNum, int drawNum, int H, int simuNum, Random random)
        {
            int count = 0;
            List<bool> deck = CreateDeck(deckNum, hitNum);
            for (int i = 0; i < simuNum; i++)
            {
                deck = DeckShuffle(deck);
                if (DrawHitNum(deck, drawNum) == H)
                {
                    count += 1;
                }
            }
            return ((float)count / (float)simuNum) * 100;
        }
    }
}
