﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace monteCarlo
{
    public partial class Form1 : Form
    {
        Random random = new Random();
        public Form1()
        {
            InitializeComponent();
        }

        private void pokemonProbSimu()
        {
            int percent = int.Parse(this.percentTextBox.Text);
            int simuNum = int.Parse(this.simuNumTextBox.Text);
            float result;

            if (continuousRadioButton.Checked)
            {
                int N = int.Parse(this.continuousHitTextBox.Text);
                result = monteCarlo.Pattern1(percent, N, simuNum, this.random);
            }
            else if (notHitAvgRadioButton.Checked)
            {
                result = monteCarlo.Pattern2(percent, simuNum, this.random);
            }
            else if (hitNumOverRadioButton.Checked)
            {
                int S = int.Parse(this.hitNumOverSimuNumTextBox.Text);
                int H = int.Parse(this.hitNumOverTextBox.Text);
                result = monteCarlo.Pattern3(percent, S, H, simuNum, this.random);
            }
            else
            {
                int S = int.Parse(this.hitNumEqualSimuNumTextBox.Text);
                int H = int.Parse(this.hitNumEqualTextBox.Text);
                result = monteCarlo.Pattern4(percent, S, H, simuNum, this.random);
            }
            this.resultTextBox.Text = result.ToString();
        }

        private void kardGameProbSimu()
        {
            int deckNum = int.Parse(this.deckNumTextBox.Text);
            int hitNum = int.Parse(this.deckHitNumTextBox.Text);
            int simuNum = int.Parse(this.simuNumTextBox.Text);
            float result;

            if (drawHitNumOverRadioButton.Checked)
            {
                int drawNum = int.Parse(this.drawHitNumOverDrawNumTextBox.Text);
                int H = int.Parse(this.drawHitNumOverHitNumTextBox.Text);
                result = monteCarlo.Pattern5(deckNum, hitNum, drawNum, H, simuNum, random);
            }
            else
            {
                int drawNum = int.Parse(this.drawHitNumEqualDrawNumTextBox.Text);
                int H = int.Parse(this.drawHitNumEqualHitNumTextBox.Text);
                result = monteCarlo.Pattern6(deckNum, hitNum, drawNum, H, simuNum, random);
            }
            this.resultTextBox.Text = result.ToString();
        }

        private void Simulation(object sender, EventArgs e)
        {
            if (pokemonProbRadioButton.Checked)
            {
                pokemonProbSimu();
            } else
            {
                kardGameProbSimu();
            }
        }

        private void SelectedPokemonProb(object sender, EventArgs e)
        {
            percentTextBox.Enabled = true;

            continuousRadioButton.Enabled = true;
            notHitAvgRadioButton.Enabled = true;
            hitNumOverRadioButton.Enabled = true;
            hitNumEqualRadioButton.Enabled = true;

            if (continuousRadioButton.Checked)
            {
                continuousHitTextBox.Enabled = true;
            }
            else if (hitNumOverRadioButton.Checked)
            {
                hitNumOverSimuNumTextBox.Enabled = true;
                hitNumOverTextBox.Enabled = true;
            }
            else if (hitNumEqualRadioButton.Checked)
            {
                hitNumEqualSimuNumTextBox.Enabled = true;
                hitNumEqualTextBox.Enabled = true;
            }

            deckNumTextBox.Enabled = false;
            deckHitNumTextBox.Enabled = false;

            drawHitNumOverRadioButton.Enabled = false;
            drawHitNumOverDrawNumTextBox.Enabled = false;
            drawHitNumOverHitNumTextBox.Enabled = false;

            drawHitNumEqualRadioButton.Enabled = false;
            drawHitNumEqualDrawNumTextBox.Enabled = false;
            drawHitNumEqualHitNumTextBox.Enabled = false;
        }

        private void SelectedKardGameProb(object sender, EventArgs e)
        {
            percentTextBox.Enabled = false;
            continuousRadioButton.Enabled = false;
            continuousHitTextBox.Enabled = false;

            notHitAvgRadioButton.Enabled = false;

            hitNumOverRadioButton.Enabled = false;
            hitNumOverSimuNumTextBox.Enabled = false;
            hitNumOverTextBox.Enabled = false;

            hitNumEqualRadioButton.Enabled = false;
            hitNumEqualSimuNumTextBox.Enabled = false;
            hitNumEqualTextBox.Enabled = false;

            deckNumTextBox.Enabled = true;
            deckHitNumTextBox.Enabled = true;

            drawHitNumOverRadioButton.Enabled = true;
            drawHitNumEqualRadioButton.Enabled = true;

            if (drawHitNumOverRadioButton.Checked)
            {
                drawHitNumOverDrawNumTextBox.Enabled = true;
                drawHitNumOverHitNumTextBox.Enabled = true;
            } else if (drawHitNumEqualRadioButton.Checked)
            {
                drawHitNumEqualDrawNumTextBox.Enabled = true;
                drawHitNumEqualHitNumTextBox.Enabled = true;
            }
        }

        private void SelectedContinuousHitPercent(object sender, EventArgs e)
        {
            continuousHitTextBox.Enabled = true;
            hitNumOverSimuNumTextBox.Enabled = false;
            hitNumOverTextBox.Enabled = false;
            hitNumEqualSimuNumTextBox.Enabled = false;
            hitNumEqualTextBox.Enabled = false;
        }

        private void SelectedNotHitAvgNum(object sender, EventArgs e)
        {
            continuousHitTextBox.Enabled = false;
            hitNumOverSimuNumTextBox.Enabled = false;
            hitNumOverTextBox.Enabled = false;
            hitNumEqualSimuNumTextBox.Enabled = false;
            hitNumEqualTextBox.Enabled = false;
        }

        private void SelectedhitNumOverPercent(object sender, EventArgs e)
        {
            continuousHitTextBox.Enabled = false;
            hitNumOverSimuNumTextBox.Enabled = true;
            hitNumOverTextBox.Enabled = true;
            hitNumEqualSimuNumTextBox.Enabled = false;
            hitNumEqualTextBox.Enabled = false;
        }

        private void SelectedHitNumEqualPercent(object sender, EventArgs e)
        {
            continuousHitTextBox.Enabled = false;
            hitNumOverSimuNumTextBox.Enabled = false;
            hitNumOverTextBox.Enabled = false;
            hitNumEqualSimuNumTextBox.Enabled = true;
            hitNumEqualTextBox.Enabled = true;
        }

        private void SelectedDrawHitNumOver(object sender, EventArgs e)
        {
            drawHitNumOverDrawNumTextBox.Enabled = true;
            drawHitNumOverHitNumTextBox.Enabled = true;

            drawHitNumEqualDrawNumTextBox.Enabled = false;
            drawHitNumEqualHitNumTextBox.Enabled = false;
        }

        private void SelectedDrawHitNumEqual(object sender, EventArgs e)
        {
            drawHitNumOverDrawNumTextBox.Enabled = false;
            drawHitNumOverHitNumTextBox.Enabled = false;

            drawHitNumEqualDrawNumTextBox.Enabled = true;
            drawHitNumEqualHitNumTextBox.Enabled = true;
        }
    }
}
