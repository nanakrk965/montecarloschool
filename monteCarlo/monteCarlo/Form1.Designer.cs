﻿namespace monteCarlo
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.startSimuButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.percentTextBox = new System.Windows.Forms.MaskedTextBox();
            this.continuousHitTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.resultTextBox = new System.Windows.Forms.TextBox();
            this.simuNumTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.hitNumOverSimuNumTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.continuousRadioButton = new System.Windows.Forms.RadioButton();
            this.notHitAvgRadioButton = new System.Windows.Forms.RadioButton();
            this.hitNumOverRadioButton = new System.Windows.Forms.RadioButton();
            this.pokemonGroupBox = new System.Windows.Forms.GroupBox();
            this.hitNumEqualRadioButton = new System.Windows.Forms.RadioButton();
            this.hitNumEqualSimuNumTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.hitNumOverTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.hitNumEqualTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.deckNumTextBox = new System.Windows.Forms.MaskedTextBox();
            this.deckHitNumTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.pokemonProbRadioButton = new System.Windows.Forms.RadioButton();
            this.kardGameProbRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.drawHitNumOverRadioButton = new System.Windows.Forms.RadioButton();
            this.drawHitNumOverDrawNumTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.drawHitNumOverHitNumTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.drawHitNumEqualRadioButton = new System.Windows.Forms.RadioButton();
            this.drawHitNumEqualDrawNumTextBox = new System.Windows.Forms.MaskedTextBox();
            this.drawHitNumEqualHitNumTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.pokemonGroupBox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // startSimuButton
            // 
            this.startSimuButton.Location = new System.Drawing.Point(6, 19);
            this.startSimuButton.Name = "startSimuButton";
            this.startSimuButton.Size = new System.Drawing.Size(149, 129);
            this.startSimuButton.TabIndex = 0;
            this.startSimuButton.Text = "シュミレーション開始";
            this.startSimuButton.UseVisualStyleBackColor = true;
            this.startSimuButton.Click += new System.EventHandler(this.Simulation);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(138, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "パーセント";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(142, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "回連続で当たる確率";
            // 
            // percentTextBox
            // 
            this.percentTextBox.Location = new System.Drawing.Point(36, 22);
            this.percentTextBox.Mask = "00";
            this.percentTextBox.Name = "percentTextBox";
            this.percentTextBox.Size = new System.Drawing.Size(100, 19);
            this.percentTextBox.TabIndex = 3;
            // 
            // continuousHitTextBox
            // 
            this.continuousHitTextBox.Location = new System.Drawing.Point(36, 15);
            this.continuousHitTextBox.Mask = "000";
            this.continuousHitTextBox.Name = "continuousHitTextBox";
            this.continuousHitTextBox.Size = new System.Drawing.Size(100, 19);
            this.continuousHitTextBox.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(161, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "シュミレーション回数";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(161, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "実行結果→";
            // 
            // resultTextBox
            // 
            this.resultTextBox.Enabled = false;
            this.resultTextBox.Location = new System.Drawing.Point(265, 67);
            this.resultTextBox.Name = "resultTextBox";
            this.resultTextBox.Size = new System.Drawing.Size(100, 19);
            this.resultTextBox.TabIndex = 8;
            // 
            // simuNumTextBox
            // 
            this.simuNumTextBox.Location = new System.Drawing.Point(265, 34);
            this.simuNumTextBox.Mask = "00000000";
            this.simuNumTextBox.Name = "simuNumTextBox";
            this.simuNumTextBox.Size = new System.Drawing.Size(100, 19);
            this.simuNumTextBox.TabIndex = 11;
            this.simuNumTextBox.Text = "12800";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(34, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(239, 12);
            this.label6.TabIndex = 12;
            this.label6.Text = "外れるまで試行し続けて、初めて外れる平均回数";
            // 
            // hitNumOverSimuNumTextBox
            // 
            this.hitNumOverSimuNumTextBox.Enabled = false;
            this.hitNumOverSimuNumTextBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.hitNumOverSimuNumTextBox.Location = new System.Drawing.Point(36, 85);
            this.hitNumOverSimuNumTextBox.Mask = "000";
            this.hitNumOverSimuNumTextBox.Name = "hitNumOverSimuNumTextBox";
            this.hitNumOverSimuNumTextBox.Size = new System.Drawing.Size(100, 19);
            this.hitNumOverSimuNumTextBox.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(142, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 12);
            this.label7.TabIndex = 14;
            this.label7.Text = "回試行して";
            // 
            // continuousRadioButton
            // 
            this.continuousRadioButton.AutoSize = true;
            this.continuousRadioButton.Checked = true;
            this.continuousRadioButton.Location = new System.Drawing.Point(16, 18);
            this.continuousRadioButton.Name = "continuousRadioButton";
            this.continuousRadioButton.Size = new System.Drawing.Size(14, 13);
            this.continuousRadioButton.TabIndex = 15;
            this.continuousRadioButton.TabStop = true;
            this.continuousRadioButton.UseVisualStyleBackColor = true;
            this.continuousRadioButton.CheckedChanged += new System.EventHandler(this.SelectedContinuousHitPercent);
            // 
            // notHitAvgRadioButton
            // 
            this.notHitAvgRadioButton.AutoSize = true;
            this.notHitAvgRadioButton.Location = new System.Drawing.Point(16, 55);
            this.notHitAvgRadioButton.Name = "notHitAvgRadioButton";
            this.notHitAvgRadioButton.Size = new System.Drawing.Size(14, 13);
            this.notHitAvgRadioButton.TabIndex = 16;
            this.notHitAvgRadioButton.UseVisualStyleBackColor = true;
            this.notHitAvgRadioButton.CheckedChanged += new System.EventHandler(this.SelectedNotHitAvgNum);
            // 
            // hitNumOverRadioButton
            // 
            this.hitNumOverRadioButton.AutoSize = true;
            this.hitNumOverRadioButton.Location = new System.Drawing.Point(16, 87);
            this.hitNumOverRadioButton.Name = "hitNumOverRadioButton";
            this.hitNumOverRadioButton.Size = new System.Drawing.Size(14, 13);
            this.hitNumOverRadioButton.TabIndex = 17;
            this.hitNumOverRadioButton.UseVisualStyleBackColor = true;
            this.hitNumOverRadioButton.CheckedChanged += new System.EventHandler(this.SelectedhitNumOverPercent);
            // 
            // pokemonGroupBox
            // 
            this.pokemonGroupBox.Controls.Add(this.hitNumEqualRadioButton);
            this.pokemonGroupBox.Controls.Add(this.continuousRadioButton);
            this.pokemonGroupBox.Controls.Add(this.hitNumOverRadioButton);
            this.pokemonGroupBox.Controls.Add(this.notHitAvgRadioButton);
            this.pokemonGroupBox.Controls.Add(this.continuousHitTextBox);
            this.pokemonGroupBox.Controls.Add(this.label2);
            this.pokemonGroupBox.Controls.Add(this.label10);
            this.pokemonGroupBox.Controls.Add(this.label6);
            this.pokemonGroupBox.Controls.Add(this.hitNumEqualTextBox);
            this.pokemonGroupBox.Controls.Add(this.hitNumOverSimuNumTextBox);
            this.pokemonGroupBox.Controls.Add(this.label9);
            this.pokemonGroupBox.Controls.Add(this.label7);
            this.pokemonGroupBox.Controls.Add(this.label8);
            this.pokemonGroupBox.Controls.Add(this.hitNumEqualSimuNumTextBox);
            this.pokemonGroupBox.Controls.Add(this.hitNumOverTextBox);
            this.pokemonGroupBox.Location = new System.Drawing.Point(36, 59);
            this.pokemonGroupBox.Name = "pokemonGroupBox";
            this.pokemonGroupBox.Size = new System.Drawing.Size(421, 156);
            this.pokemonGroupBox.TabIndex = 18;
            this.pokemonGroupBox.TabStop = false;
            this.pokemonGroupBox.Text = "ポケモン風";
            // 
            // hitNumEqualRadioButton
            // 
            this.hitNumEqualRadioButton.AutoSize = true;
            this.hitNumEqualRadioButton.Location = new System.Drawing.Point(16, 124);
            this.hitNumEqualRadioButton.Name = "hitNumEqualRadioButton";
            this.hitNumEqualRadioButton.Size = new System.Drawing.Size(14, 13);
            this.hitNumEqualRadioButton.TabIndex = 20;
            this.hitNumEqualRadioButton.TabStop = true;
            this.hitNumEqualRadioButton.UseVisualStyleBackColor = true;
            this.hitNumEqualRadioButton.CheckedChanged += new System.EventHandler(this.SelectedHitNumEqualPercent);
            // 
            // hitNumEqualSimuNumTextBox
            // 
            this.hitNumEqualSimuNumTextBox.Enabled = false;
            this.hitNumEqualSimuNumTextBox.Location = new System.Drawing.Point(36, 121);
            this.hitNumEqualSimuNumTextBox.Mask = "000";
            this.hitNumEqualSimuNumTextBox.Name = "hitNumEqualSimuNumTextBox";
            this.hitNumEqualSimuNumTextBox.Size = new System.Drawing.Size(100, 19);
            this.hitNumEqualSimuNumTextBox.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(169, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 12);
            this.label5.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(320, 87);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 12);
            this.label8.TabIndex = 21;
            this.label8.Text = "回以上当たる確率";
            // 
            // hitNumOverTextBox
            // 
            this.hitNumOverTextBox.Enabled = false;
            this.hitNumOverTextBox.Location = new System.Drawing.Point(214, 84);
            this.hitNumOverTextBox.Mask = "000";
            this.hitNumOverTextBox.Name = "hitNumOverTextBox";
            this.hitNumOverTextBox.Size = new System.Drawing.Size(100, 19);
            this.hitNumOverTextBox.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(142, 125);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 12);
            this.label9.TabIndex = 23;
            this.label9.Text = "回試行して";
            // 
            // hitNumEqualTextBox
            // 
            this.hitNumEqualTextBox.Enabled = false;
            this.hitNumEqualTextBox.Location = new System.Drawing.Point(214, 121);
            this.hitNumEqualTextBox.Mask = "000";
            this.hitNumEqualTextBox.Name = "hitNumEqualTextBox";
            this.hitNumEqualTextBox.Size = new System.Drawing.Size(100, 19);
            this.hitNumEqualTextBox.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(320, 124);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 12);
            this.label10.TabIndex = 25;
            this.label10.Text = "回丁度当たる確率";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(138, 255);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 12);
            this.label11.TabIndex = 26;
            this.label11.Text = "枚の中";
            // 
            // deckNumTextBox
            // 
            this.deckNumTextBox.Enabled = false;
            this.deckNumTextBox.Location = new System.Drawing.Point(36, 252);
            this.deckNumTextBox.Mask = "000";
            this.deckNumTextBox.Name = "deckNumTextBox";
            this.deckNumTextBox.Size = new System.Drawing.Size(100, 19);
            this.deckNumTextBox.TabIndex = 27;
            // 
            // deckHitNumTextBox
            // 
            this.deckHitNumTextBox.Enabled = false;
            this.deckHitNumTextBox.Location = new System.Drawing.Point(180, 252);
            this.deckHitNumTextBox.Mask = "000";
            this.deckHitNumTextBox.Name = "deckHitNumTextBox";
            this.deckHitNumTextBox.Size = new System.Drawing.Size(100, 19);
            this.deckHitNumTextBox.TabIndex = 28;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(286, 255);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 12);
            this.label12.TabIndex = 29;
            this.label12.Text = "枚当たりがある";
            // 
            // pokemonProbRadioButton
            // 
            this.pokemonProbRadioButton.AutoSize = true;
            this.pokemonProbRadioButton.Checked = true;
            this.pokemonProbRadioButton.Location = new System.Drawing.Point(12, 25);
            this.pokemonProbRadioButton.Name = "pokemonProbRadioButton";
            this.pokemonProbRadioButton.Size = new System.Drawing.Size(14, 13);
            this.pokemonProbRadioButton.TabIndex = 30;
            this.pokemonProbRadioButton.TabStop = true;
            this.pokemonProbRadioButton.UseVisualStyleBackColor = true;
            this.pokemonProbRadioButton.CheckedChanged += new System.EventHandler(this.SelectedPokemonProb);
            // 
            // kardGameProbRadioButton
            // 
            this.kardGameProbRadioButton.AutoSize = true;
            this.kardGameProbRadioButton.Location = new System.Drawing.Point(12, 255);
            this.kardGameProbRadioButton.Name = "kardGameProbRadioButton";
            this.kardGameProbRadioButton.Size = new System.Drawing.Size(14, 13);
            this.kardGameProbRadioButton.TabIndex = 31;
            this.kardGameProbRadioButton.UseVisualStyleBackColor = true;
            this.kardGameProbRadioButton.CheckedChanged += new System.EventHandler(this.SelectedKardGameProb);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.startSimuButton);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.simuNumTextBox);
            this.groupBox2.Controls.Add(this.resultTextBox);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(12, 434);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(521, 154);
            this.groupBox2.TabIndex = 32;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.drawHitNumEqualHitNumTextBox);
            this.groupBox3.Controls.Add(this.drawHitNumEqualDrawNumTextBox);
            this.groupBox3.Controls.Add(this.drawHitNumEqualRadioButton);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.drawHitNumOverHitNumTextBox);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.drawHitNumOverDrawNumTextBox);
            this.groupBox3.Controls.Add(this.drawHitNumOverRadioButton);
            this.groupBox3.Location = new System.Drawing.Point(36, 297);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(480, 100);
            this.groupBox3.TabIndex = 33;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "カードゲーム風";
            // 
            // drawHitNumOverRadioButton
            // 
            this.drawHitNumOverRadioButton.AutoSize = true;
            this.drawHitNumOverRadioButton.Checked = true;
            this.drawHitNumOverRadioButton.Enabled = false;
            this.drawHitNumOverRadioButton.Location = new System.Drawing.Point(16, 18);
            this.drawHitNumOverRadioButton.Name = "drawHitNumOverRadioButton";
            this.drawHitNumOverRadioButton.Size = new System.Drawing.Size(14, 13);
            this.drawHitNumOverRadioButton.TabIndex = 34;
            this.drawHitNumOverRadioButton.TabStop = true;
            this.drawHitNumOverRadioButton.UseVisualStyleBackColor = true;
            this.drawHitNumOverRadioButton.CheckedChanged += new System.EventHandler(this.SelectedDrawHitNumOver);
            // 
            // drawHitNumOverDrawNumTextBox
            // 
            this.drawHitNumOverDrawNumTextBox.Enabled = false;
            this.drawHitNumOverDrawNumTextBox.Location = new System.Drawing.Point(36, 15);
            this.drawHitNumOverDrawNumTextBox.Name = "drawHitNumOverDrawNumTextBox";
            this.drawHitNumOverDrawNumTextBox.Size = new System.Drawing.Size(100, 19);
            this.drawHitNumOverDrawNumTextBox.TabIndex = 35;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(144, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 12);
            this.label13.TabIndex = 36;
            this.label13.Text = "枚引いて";
            // 
            // drawHitNumOverHitNumTextBox
            // 
            this.drawHitNumOverHitNumTextBox.Enabled = false;
            this.drawHitNumOverHitNumTextBox.Location = new System.Drawing.Point(198, 15);
            this.drawHitNumOverHitNumTextBox.Name = "drawHitNumOverHitNumTextBox";
            this.drawHitNumOverHitNumTextBox.Size = new System.Drawing.Size(100, 19);
            this.drawHitNumOverHitNumTextBox.TabIndex = 37;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(304, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(174, 12);
            this.label14.TabIndex = 38;
            this.label14.Text = "枚以上の当たりが含まれている確率";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(146, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 12);
            this.label15.TabIndex = 39;
            this.label15.Text = "枚引いて";
            // 
            // drawHitNumEqualRadioButton
            // 
            this.drawHitNumEqualRadioButton.AutoSize = true;
            this.drawHitNumEqualRadioButton.Enabled = false;
            this.drawHitNumEqualRadioButton.Location = new System.Drawing.Point(16, 52);
            this.drawHitNumEqualRadioButton.Name = "drawHitNumEqualRadioButton";
            this.drawHitNumEqualRadioButton.Size = new System.Drawing.Size(14, 13);
            this.drawHitNumEqualRadioButton.TabIndex = 40;
            this.drawHitNumEqualRadioButton.TabStop = true;
            this.drawHitNumEqualRadioButton.UseVisualStyleBackColor = true;
            this.drawHitNumEqualRadioButton.CheckedChanged += new System.EventHandler(this.SelectedDrawHitNumEqual);
            // 
            // drawHitNumEqualDrawNumTextBox
            // 
            this.drawHitNumEqualDrawNumTextBox.Enabled = false;
            this.drawHitNumEqualDrawNumTextBox.Location = new System.Drawing.Point(36, 49);
            this.drawHitNumEqualDrawNumTextBox.Name = "drawHitNumEqualDrawNumTextBox";
            this.drawHitNumEqualDrawNumTextBox.Size = new System.Drawing.Size(100, 19);
            this.drawHitNumEqualDrawNumTextBox.TabIndex = 41;
            // 
            // drawHitNumEqualHitNumTextBox
            // 
            this.drawHitNumEqualHitNumTextBox.Enabled = false;
            this.drawHitNumEqualHitNumTextBox.Location = new System.Drawing.Point(198, 49);
            this.drawHitNumEqualHitNumTextBox.Name = "drawHitNumEqualHitNumTextBox";
            this.drawHitNumEqualHitNumTextBox.Size = new System.Drawing.Size(100, 19);
            this.drawHitNumEqualHitNumTextBox.TabIndex = 42;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(304, 52);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(174, 12);
            this.label16.TabIndex = 43;
            this.label16.Text = "枚丁度の当たりが含まれている確率";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 588);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.kardGameProbRadioButton);
            this.Controls.Add(this.pokemonProbRadioButton);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.deckHitNumTextBox);
            this.Controls.Add(this.deckNumTextBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pokemonGroupBox);
            this.Controls.Add(this.percentTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.pokemonGroupBox.ResumeLayout(false);
            this.pokemonGroupBox.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startSimuButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox percentTextBox;
        private System.Windows.Forms.MaskedTextBox continuousHitTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox resultTextBox;
        private System.Windows.Forms.MaskedTextBox simuNumTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox hitNumOverSimuNumTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton continuousRadioButton;
        private System.Windows.Forms.RadioButton notHitAvgRadioButton;
        private System.Windows.Forms.RadioButton hitNumOverRadioButton;
        private System.Windows.Forms.GroupBox pokemonGroupBox;
        private System.Windows.Forms.MaskedTextBox hitNumEqualSimuNumTextBox;
        private System.Windows.Forms.RadioButton hitNumEqualRadioButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox hitNumOverTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox hitNumEqualTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox deckNumTextBox;
        private System.Windows.Forms.MaskedTextBox deckHitNumTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton pokemonProbRadioButton;
        private System.Windows.Forms.RadioButton kardGameProbRadioButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox drawHitNumOverDrawNumTextBox;
        private System.Windows.Forms.RadioButton drawHitNumOverRadioButton;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox drawHitNumOverHitNumTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MaskedTextBox drawHitNumEqualHitNumTextBox;
        private System.Windows.Forms.MaskedTextBox drawHitNumEqualDrawNumTextBox;
        private System.Windows.Forms.RadioButton drawHitNumEqualRadioButton;
        private System.Windows.Forms.Label label15;
    }
}

